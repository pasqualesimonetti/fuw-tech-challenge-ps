# fuw-tech-challenge-ps

## Demo

The solution has already been deployed on [Heroku](https://heroku.com/) (_backend_) and [Vercel](https://vercel.com/) (_frontend_): 

- Task 1: 
    * https://fuw-tech-challenge-ps.vercel.app/company/novartis
    * https://fuw-tech-challenge-ps.vercel.app/company/bmw
    * https://fuw-tech-challenge-ps.vercel.app/company/apple

- Task 2: 
    * https://fuw-tech-challenge-ps.vercel.app/index/smi
    * https://fuw-tech-challenge-ps.vercel.app/index/dax
    * https://fuw-tech-challenge-ps.vercel.app/index/dow

## Technical setup

### Clone Repo

Add permissions, and ssh keys to gitlab.

Then clone the repository:

```bash
cd yourdrive/yourdir
mkdir fuw-tech-challenge-ps
cd fuw-tech-challenge-ps
git clone --recurse-submodules git@gitlab.com:pasqualesimonetti/fuw-tech-challenge-ps.git .
```

### Get Docker

Install Docker (including docker-compose)

Build Docker image:

```bash
cd yourdrive/yourdir/fuw-tech-challenge-ps
docker-compose build
```

Run Docker image:

(Make sure to stop other local webservers on localhost, port 8001 and 3001)

```
docker-compose up -d
```
### Local run
(Make sure to stop other local webservers on localhost, port 8001 and 3001)

backend
```bash
# with PHP 8.2 installed
cd ./backend
php -S 127.0.0.1:8001 -t public
```

frontend
```bash
# with Node.js 18.2 and yarn installed
cd ./frontend
echo 'BACKEND_PATH=http://127.0.0.1:8001' > .env.local
yarn install
# then
yarn dev
# or 
yarn build
yarn start
```

## Tasks

For fast prototyping purposes, and to prove my development skills, I have created a _backend_ with [Lumen](https://lumen.laravel.com/) PHP framework and a _frontend_ made with [Next.js](https://nextjs.org/) and [TypeScript](https://www.typescriptlang.org/).

Dummy DB and API with mock data have been included in the _backend_.

This solution is not perfect, of course, but perfectible with a bit more time.


### Task 1

An HTML page that shows all available infos about Novartis.

* Go to: http://localhost:3001/company/novartis


### Task 2

An HTML page that lists all the companies, that are included
in the swiss stock market index SMI, containing links to the pages
created in Task 1.

* Go to: http://localhost:3001/index/smi


### Plus

* [Material UI](https://mui.com/) styled-components have been used
* Testing:

backend
```bash
# on Docker
docker exec -it backend /bin/bash
./vendor/bin/phpunit

# or locally
cd ./backend
./vendor/bin/phpunit
```

frontend
```bash
# on Docker
docker exec -it frontend /bin/bash
yarn lint

# or locally (better)
cd ./frontend
yarn install
yarn lint
yarn cypress
```


